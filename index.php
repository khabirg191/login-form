<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.rtl.min.css" integrity="sha384-OXTEbYDqaX2ZY/BOaZV/yFGChYHtrXH2nyXJ372n2Y8abBhrqacCEe+3qhSHtLjy" crossorigin="anonymous">

  <title>Contact Form</title>
</head>

<style media="screen">
  body {
    background: #504477;
    color: #FFF;
  }

  .container {
    padding-top: 20px;
  }

  .table {
    color: #FFF;
  }
</style>

<body>

  <div class="container">
    <?php

    if (isset($_POST['send'])) :

      if (in_array(NULL, $_POST)) :
        
    ?>
        <div class="alert alert-primary text-center" role="alert">
          Something is missing
        </div>
        <p><a href="./">Try again</a></p>

      <?php else : extract($_POST);

        require_once("write.php");

        require_once("read.php");

      ?>

        <p>
          <a href="./">Go back</a>
        </p>

      <?php endif;
    else : ?>

      <h1 class="text-center">Contact Form</h1>
      <form action="./" method="post">
        <div class="form-group mb-3">
          <input type="text" name="name" class="form-control" placeholder="Name" aria-label="Name">
        </div>
        <div class="form-group mb-3">
          <input type="text" name="email" class="form-control" placeholder="E-mail" aria-label="Email">
        </div>
        <div class="form-group mb-3">
          <textarea type="text" name="message" class="form-control" placeholder="Your message here..." aria-label="With textarea"></textarea>
        </div>
        <button type="submit" class="btn btn-primary" name="send" value="Send">Send</button>
      </form>

    <?php endif; ?>

  </div>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

</body>

</html>