<table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Message</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (file_exists("data.txt")) {

              $fp = fopen("data.txt", "r");
              $i = 1;  
              while (!feof($fp)) {
                $current_string = fgets($fp);
                if (!empty($current_string)) {
                  $ex = explode(";", $current_string);
            ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $ex[0]; ?></td>
                    <td><?php echo $ex[1]; ?></td>
                    <td><?php echo $ex[2]; ?></td>
                  </tr>
            <?php
                  $i++;
                }
              }

              fclose($fp);

            } else {
              echo "File does not exist.";
            }
            ?>

          </tbody>
        </table>